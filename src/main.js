/****************************************************
 * Program: main.js
 * 
 * Use: This program will facilitate getting and saving records in the listings
 *  elasticsearch index.
 * 
 *  All sensitive data is retrieved from .env via dotenv
 *  
 *  Elasticsearch will be used to handle all CRUD functionality 
 *  We will simulate using a real database by using proper authentification
 *      to connect to the local server.
 * 
 * Express server will display available locations.
 * 
 */

const express = require("express")
const app = express()
const elasticSearch = require('@elastic/elasticsearch')
const fs = require('fs')

//grab and parse env file via dotenv.
require('dotenv').config()

const expressPort = process.env.EXPRESS_PORT
const expressRoute = process.env.EXPRESS_ROUTE
const esHost = process.env.ES_HOST
const esPort = process.env.ES_PORT
const esUsername = process.env.ES_USERNAME
const esPassword = process.env.ES_PASSWORD
const indexName = process.env.INDEX_NAME

const esNode = "http://" + esHost + ':' + esPort

console.log("esNode is:", esNode)
console.log("port is:", expressPort)
console.log("route is:", expressRoute)

//Connect to elasticsearch server
const client = new elasticSearch.Client({
    node: esNode,
    auth: {
        username: esUsername,
        password: esPassword
    },
    apiVersion: '7.13.0'
})


//Parse request JSON list (would be contacting server here instead)
//This JSON parse simulates gathering data from a server and setting ID's based on the count of items being sent in.
//It's not perfect, but gives each starting shard a unique ID that is easy to use.
try{
    var JSONListings = JSON.parse(fs.readFileSync('request.json'))
    console.log("Parsing JSON")
    console.log(JSONListings)

    var JSONcount = Object.keys(JSONListings.listings).length
    console.log(JSONcount)
    
    for(i = 0; i < JSONcount; i++){
        //Send parsed data to esPOST to add to listings index on Elasticsearch
        var address = JSONListings.listings[i].address
        var age = JSONListings.listings[i].ageInDays
        var isDeleted = JSONListings.listings[i].isDeleted
        var propertyType = JSONListings.listings[i].propertyType

        
        esPOST(indexName, i, address, age, isDeleted, propertyType)
        
    }

    waitandGET()

} catch(err){
    console.log("Json parsing failed")
    console.error(err)
}


//OPTIONAL FUNCTIONS. I threw these in to show functionality. Comment out if you test another way!
esDELETE(indexName, 3)
esGETbyID(indexName, 4)
esPATCH(indexName, 2, 'short')


app.listen(expressPort)




/********************************
 * Function: expressGET
 * 
 * Use: This function will take any input and display it to the express server
 * 
 * Arguments: input
 * 
 * Returns: Nothing
 *******************************/

function expressGET(input){
    app.get(expressRoute, function (req, res) {
        res.send(input)
    })
}

/********************************
 * Function: esGETAvailable
 * 
 * Use: This function retrieves at most 20 items (excluding deleted)
 *  from the selected elasticsearch index
 * 
 * Arguments: indexName
 * 
 * Returns: Nothing
 *******************************/

async function esGETAvailable(indexName){
    const {body} = await client.search({
        index: indexName,
        size: 20,
        body:{
            query:{
                match:{
                    isDeleted: 'false'
                }
            }
        }
    }).catch(err => {
        console.log("Failed to retrieve index.")
        console.log(err)
        expressGET("Failed to retrieve index.")
    })

    console.log("GETAvailable search results")
    console.log(body.hits.hits) 
    expressGET(body.hits.hits) 
}

/********************************
 * Function: esPOST
 * 
 * Use: This function adds another shard to the elasticsearch index
 *  if the index is not yet created, create the index, then add the new shard
 * 
 * Arguments: indexName, id, address, ageInDays, isDeleted, propertyType
 * 
 * Returns: Nothing
 *******************************/

async function esPOST(indexName, id, address, ageInDays, isDeleted, propertyType){
    const {body} = await client.index({
            index: indexName,
            id: id,
            refresh: true,
            body:{
                address: address,
                ageInDays: ageInDays,
                isDeleted: isDeleted,
                propertyType: propertyType
            }
        }).catch(err => {
            console.log("esPOST failed.")
            console.log(err)
        })
    console.log("esPOST results")
    console.log(body)
}


//BELOW ARE OPTIONAL


/********************************
 * Function: esGETbyID
 * 
 * Use: This function retrieves a selected item from the elasticsearch index
 *  data is found via ID
 * 
 * Arguments: index, id
 * 
 * Returns: Nothing
 *******************************/

async function esGETbyID(indexName, id){
    const {body} = await client.get({
        index: indexName,
        id: id
    }).catch(err => {
        console.log("Get by ID failed")
        console.log(err)
    })
    console.log("GET by ID results")
    console.log(body)
}

/********************************
 * Function: esPATCH
 * 
 * Use: This function modifies arbitrary body JSON
 *  The shard is found via ID
 * 
 * Arguments: index, id, type (last argument can be swapped out for other options or something more robust)
 * 
 * Returns: Nothing
 *******************************/

async function esPATCH(indexName, id, type){
    const {body} = await client.update({
        index: indexName,
        id: id,
        refresh: true,
        body:{
            doc:{
                properties:{
                    ageInDays:{
                        type: type
                    }
                }
            }
        }
    }).catch(err => {
        console.log("esPATCH failed")
        console.log(err)
    })
    console.log("esPATCH results")
    console.log(body)
}


/********************************
 * Function: esDELETE
 * 
 * Use: This function will set isDeleted to true on the given ID within the
 *  specified index
 *  Won't ACTUALLY delete the data.
 * 
 * Arguments: index, id
 * 
 * Returns: Nothing
 *******************************/

async function esDELETE(indexName, id){
    const {body} = await client.update({
        index: indexName,
        id: id,
        body:{
            doc:{
                isDeleted: 'true'
            }
        }
    }).catch(err => {
        console.log("Delete failed.")
        console.log(err)
    })

    console.log("Delete succeeded!")
    console.log(body)
}



/********************************
 * Function: Sleep
 * 
 * Use: This function will pause the program for the given ms.
 * 
 * Arguments: ms
 * 
 * Returns: Nothing
 *******************************/


async function sleep(ms){
    return new Promise(resolve => setTimeout(resolve, ms))
    .catch(err => {
        console.log("Sleep failed")
        console.log(err)
    })
}

/********************************
 * Function: waitandGET
 * 
 * Use: This function will pause for 2 seconds before executing esGETAvailable
 *  This one probably isn't necessary. The intention is to wait for the index to update
 *  before calling GET or SEARCH
 * 
 * Arguments: None
 * 
 * Returns: Nothing
 *******************************/

function waitandGET(){
    sleep(2000).catch
    esGETAvailable(indexName)
}
